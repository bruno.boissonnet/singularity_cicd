# singularity_CICD


## Name

Singularity avec GitLab CI/CD

## Description

Projet de test : création d'une image singularity par intégration/déploiement continu de GitLab

Projet créé à partir des fichiers de Jacques LAGNEL ici : https://forgemia.inra.fr/formationcalcul2023/formation-singularity/-/tree/master/TP3_CICD


## Installation

Pour installer l'image Singularity, il faut la télécharger sur le serveur de calcul :

    singularity pull R.4.2.2_mkl.sif oras://registry.forgemia.inra.fr/bruno.boissonnet/singularity_cicd/singularity_cicd:latest

## Usage

Une fois l'image installée sur le serveur de calcul, on lance l'image :

    ./R.4.2.2_mkl.sif

